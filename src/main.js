import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { router } from './router'
import CxltToastr from 'cxlt-vue2-toastr'
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css'

// import swal from 'sweetalert';


import { HasError, AlertError } from 'vform'

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

var toastrConfigs = {
    position: 'top right',
    showDuration: 1000,
    timeOut: 5000,
    clodeButton: true,
    showMethod: 'fadein',
    hideMethod: 'fadeout'
}
Vue.use(CxltToastr, toastrConfigs)



Vue.config.productionTip = false

Vue.use(VueAxios, axios)
    // Vue.use(swal)

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
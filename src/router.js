import Vue from 'vue'
import VueRouter from 'vue-router'

const routes = [{
        path: '/',
        name: 'dashboard',
        component: () =>
            import ('./components/Dashboard'),
    },
    {
        path: '/createblog',
        component: () =>
            import ('./components/CreateBlog')
    },
    {
        path: '/bloglist',
        name: 'bloglist',
        component: () =>
            import ('./components/BlogList')
    },
    {
        path: '/blogedit/:id',
        name: 'blogedit',
        component: () =>
            import ('./components/BlogEdit')
    },
]




Vue.use(VueRouter)

export const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history',
})